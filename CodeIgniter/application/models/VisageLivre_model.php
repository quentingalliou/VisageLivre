<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// -----------------------------------------------------------------------------

class VisageLivre_model extends CI_Model
{
	/**
	 *	Insère une nouvelle ligne dans la base de données.
	 */
	public function create( $table, $options_echappees = array(), $options_non_echappees = array())
    {
        //	Vérification des données à insérer
        if(empty($options_echappees) AND empty($options_non_echappees))
        {
            return false;
        }

        return (bool) $this->db->set($options_echappees)
                                   ->set($options_non_echappees, null, false)
                                   ->insert($table);
    }

	/**
	 *	Récupère des données dans la base de données.
	 */
	public function read($table,$select = '*', $where = array(),$orderByCol = null,$orderByDir = null, $nb = null, $debut = null)
    {
	   return $this->db->select($select)
                        ->from($table)
                        ->where($where)
                        ->limit($nb, $debut)
                        ->order_by($orderByCol,$orderByDir)
                        ->get()
                        ->result();
    }
    
    /**
	 *	Récupère des données dans la base de données.
	 */
	public function readJoin($table,$select = '*', $where = array(),$tab =null,$join = null, $nb = null, $debut = null)
    {
	   return $this->db->select($select)
                        ->from($table)
                        ->join($tab,$join)
                        ->where($where)
                        ->limit($nb, $debut)
                        ->get()
                        ->result();
    }
    
    public function readColJoin($table,$select = '*', $in,$where = array(),$tab =null,$join = null,$orderByCol=null,$orderByDir=null, $nb = null, $debut = null)
    {
	   return $this->db->select($select)
                        ->from($table)
                        ->join($tab,$join)
                        ->where_in($in,$where)
                        ->limit($nb, $debut)
                        ->order_by($orderByCol,$orderByDir)
                        ->get()
                        ->result();
    }
    
    public function readJoinOrderBy($table,$select = '*', $where = array(),$tab =null,$join = null,$orderByCol=null,$orderByDir=null, $nb = null, $debut = null)
    {
	   return $this->db->select($select)
                        ->from($table)
                        ->join($tab,$join)
                        ->where($where)
                        ->limit($nb, $debut)
                        ->order_by($orderByCol,$orderByDir)
                        ->get()
                        ->result();
    }
    
    /**
	 *	Récupère la valeur max des données dans la base de données.
	 */
	public function readMax($table,$select = '*', $where = array(), $nb = null, $debut = null)
    {
	   return $this->db->select_max($select)
                        ->from($table)
                        ->where($where)
                        ->limit($nb, $debut)
                        ->get()
                        ->result();
    }
    
     /**
	 *	Récupère des données dans la base de données en fonction de plusieurs éléments pour une seule colonne.
	 */
	public function readCol($table,$select = '*',$in, $where = array(),$orderBy = null,$orderDir = null, $nb = null, $debut = null)
    {
	   return $this->db->select($select)
                        ->from($table)
                        ->where_in($in,$where)
                        ->limit($nb, $debut)
                        ->order_by($orderBy,$orderDir)
                        ->get()
                        ->result();
    }
	
	/**
	 *	Modifie une ou plusieurs lignes dans la base de données.
	 */
	
    public function update($table,$where, $options_echappees = array(), $options_non_echappees = array())
    {		
	   //	Vérification des données à mettre à jour
	   if(empty($options_echappees) AND empty($options_non_echappees))
	   {
		  return false;
       }
	
	   //	Raccourci dans le cas où on sélectionne l'id
	   if(is_integer($where))
	   {
		  $where = array('id' => $where);
	   }

	   return (bool) $this->db->set($options_echappees)
                               ->set($options_non_echappees, null, false)
                               ->where($where)
                               ->update($table);

    }
	
	/**
	 *	Supprime une ou plusieurs lignes de la base de données.
	 */
	public function delete($table,$where)
    {
	   if(is_integer($where))
	   {
		  $where = array('id' => $where);
	   }
	
	   return (bool) $this->db->where($where)
                               ->delete($table);
    }
    
    /**
	 *	Supprime une ou plusieurs lignes de la base de données en fonction de plusieurs données pour une même colonne.
	 */
	public function deleteCol($table,$in,$where)
    {
	   if(is_integer($where))
	   {
		  $where = array('id' => $where);
	   }
	
	   return (bool) $this->db->where_in($in,$where)
                               ->delete($table);
    }

	/**
	 *	Retourne le nombre de résultats.
	 */
	public function count($table,$champ = array(), $valeur = null) // Si $champ est un array, la variable $valeur sera ignorée par la méthode where()
    {
	   return (int) $this->db->where($champ, $valeur)
                              ->from($table)
                              ->count_all_results();
    }
    public function countAll($table)
	{
		return $this->db->count_all($table);
	}
}

/* End of file MY_Model.php */
/* Location: ./system/application/core/MY_Model.php */
?>
