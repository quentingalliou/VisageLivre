<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" > 
    <head>
        <title><?php echo $titre; ?></title>
        <link rel="icon" href="<?php echo img_url('favicon.ico'); ?>" />
        <meta http-equiv="Content-Type" content="text/html" charset="<?php echo $charset; ?>" />
        <link rel="stylesheet" media="screen" href="<?php echo css_url('style'); ?>" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
            <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/solid.js" integrity="sha384-+Ga2s7YBbhOD6nie0DzrZpJes+b2K1xkpKxTFFcx59QmVPaSA8c7pycsNaFwUK6l" crossorigin="anonymous"></script>
            <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js" integrity="sha384-7ox8Q2yzO/uWircfojVuCQOZl+ZZBg2D2J5nkpLqzH1HY0C1dHlTKIbpRz/LG23c" crossorigin="anonymous"></script>


    </head>
    <body>
        <div id="main">
            <nav class="navbar navbar-expand-lg navbar-light">
                <i style="margin-right:10px;" class="fa fa-book fa-3x"></i>
                <a style="color:white;" class="navbar-brand entete" href="<?php site_url('VisageLivre/home'); ?>"><b>VisageLivre</b><br />Venez échanger avec vos amis !</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <div class="navbar-nav mr-auto"></div>
                    <?php
                    if ((current_url() === site_url('VisageLivre')) || (current_url() === site_url('VisageLivre/connectUser') || (current_url() === site_url('VisageLivre/addUser')))) {
                        
                    } else {
                        ?>
                        <div>
                            <a class="btn btn-primary" href="<?php echo site_url('VisageLivre/home') ?>" role="button"><i class="fas fa-home"></i></a>
                            <a class="btn btn-primary" href="<?php echo site_url('VisageLivre/infoUser') ?>" role="button"><i class="fas fa-user-circle"></i>&nbsp;<?php echo $this->session->userdata('nickname'); ?></a>
                            <a class="btn btn-danger"  id="accueil_btn" href="<?php echo site_url('VisageLivre/disconnectUser') ?>" role="button"><i class="fas fa-sign-out-alt"></i></a>
                        </div>
                        <?php
                    }
                    ?>
            </nav>
            <br />
            <div class="container-fluid">
                <div class="row">
                    <?php echo $output; ?>
                </div>
            </div>
            <br />      
        </div>
    </body>
    <footer>
        <p>༻༺⁜࿇⁜༻༺ <br /> Quentin GALLIOU - Corentin BOSQUET - Groupe C2al - Mini-Projet Web</p>
    </footer>
</html>