<aside class="col-2 align-self-start ">
    <h5 class="sous_titre">Mes demandes d'amis</h5>
        <ul class="list-group liste">
            
            <?php
              if(empty($listRF['allRF'])){ echo "<p> Vous n'avez pas de demande d'amis pour le moment. </p>";
              
              }else{   

                foreach ($listRF['allRF'] as $user){
                    if($user->nickname!=$this->session->userdata('nickname')){
                        echo "<li class='list-group-item item_list'>$user->nickname"; ?>
                        <div>
                            <button class='buttonUser btn btn-outline-success' type='button' onclick="window.location='<?php echo site_url('VisageLivre/addRelation/'.$user->nickname);?>'">
                                <i class='fas fa-check'></i>
                            </button>
                            <button class='buttonUser btn btn-outline-danger' type='button' onclick="window.location='<?php echo site_url('VisageLivre/rmRequest/'.$user->nickname);?>'">
                                <i class='fas fa-times'></i>
                            </button>
                        </div>
                    </li>
                    <?php
                   }
                }
              }
            ?>
        </ul>
</aside>