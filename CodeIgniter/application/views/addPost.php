<div class="col-7">
    <h5 class="sous_titre">Hey ! What's up <?php echo $this->session->userdata('nickname'); ?> ?</h5>
    <div class="form-inline input-group mb-8" >
        <?php 
        $attribut = array('class' => 'col');
        echo form_open('VisageLivre/addPost', $attribut); ?>
            <div class="input-group mb-3">
                <textarea type="text" name="content" id="content" class="form-control" placeholder="Ecrivez quelque chose..." aria-label="Ecrivez quelque chose..." aria-describedby="basic-addon2"></textarea>
                <div class="input-group-append">
                    <input class="btn btn-outline-success btn-block" type="submit" value="Envoyer mon billet" />
                </div>
            </div>
        </form>
    </div>
    <div><br /></div>