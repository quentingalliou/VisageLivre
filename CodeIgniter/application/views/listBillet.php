    <div>
        <div class="nav justify-content-end">
            <div class="btn-group">
                <button id="filtre" type="button" class="btn-sm btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Filtrer
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="">Tous les billets</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="<?php echo site_url('VisageLivre/home/filtre/1');?>">Ordre chronologique</a>
                    <a class="dropdown-item" href="<?php echo site_url('VisageLivre/home/filtre/0');?>">Ordre alphabétique</a>
                </div>
            </div>
        </div>
        <div class="liste">
            <?php
            if (empty($postFriends)) {
            ?>
                <div class="form-inline input-group mb-3"  >
                    <div class="input-group-prepend">
                        <span class="input-group-text"><b>L'équipe VisageLivre</b></span>
                    </div>
                    <p class="form-control item_list">
                        Bienvenue dans le réseau social VisageLivre !
                        <br />
                        Il n'y a pas de postes, faîtes-vous vite des ami(e)s !
                    </p>
                    
                </div>
        <?php
            } else {
                foreach ($postFriends as $p) { ?>
                    <div class='form-inline input-group mb-3'  >
                        <div class='input-group-prepend'>
                            <?php
                            echo "<span class='input-group-text'><b>" . $p->auteur . "<br />" . substr($p->create_date, 0, -10) . "</b></span>"
                            ?>
                        </div>
                        <p class='form-control'>
                            <?php echo "$p->substring";
                            if(strlen($p->substring)>=30){ echo "...";} ?>
                        </p>
                        <div class='input-group-append'>
                           <button class="btn btn-outline-primary" onclick="window.location = '<?php echo site_url('VisageLivre/detailPost/').$p->iddoc; ?>'" type="button">Voir le billet</button>
                        </div>
                    </div>
            <?php
                 }
            }
            ?>
        </div>
    </div>
</div>  
