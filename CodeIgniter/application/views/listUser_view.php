<aside class="col-2 align-self-start">
        <h5 class="sous_titre">Mes listes</h5>
        <div class="liste">
        <ul class="list-group row">
            <?php  
            if ($this->uri->segment(4)!='friend'){
            foreach ($listUser['allUser'] as $user){
                    if($user->nickname!=$this->session->userdata('nickname')){
                        $fUser = array();
                        foreach ($listUser['friend'] as $f){
                            array_push($fUser, $f->friend); 
                        }
                        
                        $rFriend = array();
                        foreach ($myListRF as $r){
                            array_push($rFriend, $r->target);
                        }
                        
                       
                      if(in_array($user->nickname, $fUser)){
                          
                      echo "<li class='item_list list-group-item'>"
                        . "$user->nickname " ?>
                            <button class='buttonUser btn btn-outline-danger' type='button' onclick="window.location='<?php echo site_url('VisageLivre/rmRelation/'.$user->nickname);?>'">
                                <i class='fas fa-user-times'></i>
                            </button>
                            </li>
                            <?php
                            
                      }elseif(in_array($user->nickname, $rFriend)){
                           echo "<li class='item_list list-group-item'>"
                        . "$user->nickname " ?>
                            <button disabled class='buttonUser btn btn-outline-primary' type='button'>
                                <i class="fas fa-clock"></i>
                            </button>
                            </li>
                            <?php
                      
    
                      }else{
                        echo "<li class='item_list list-group-item'>"
                        . "$user->nickname " ?>
                                <button class='buttonUser btn btn-outline-success' type='button' onclick="window.location='<?php echo site_url('VisageLivre/addRequest/'.$user->nickname);?>'">
                                <i class="fas fa-user-plus"></i>
                                </button>
                                </li>
                                <?php
                        
                            
                        }
                    }
                }
            }else{ 
            
            foreach ($listUser['allUser'] as $user){
                    if($user->friend!=$this->session->userdata('nickname')){
                        /*$fUser = array();
                        foreach ($listUser['friend'] as $f){
                            array_push($fUser, $f->friend); 
                        }*/
                        
                        $rFriend = array();
                        foreach ($myListRF as $r){
                            array_push($rFriend, $r->target);
                        }
                        
                       
                     /* if(in_array($user->friend, $fUser)){
                          
                      echo "<li class='item_list list-group-item'>"
                        . "$user->friend " ?>
                            <button class='buttonUser btn btn-outline-danger' type='button' onclick="window.location='<?php echo site_url('VisageLivre/rmRelation/'.$user->friend);?>'">
                                <i class='fas fa-user-times'></i>
                            </button>
                            </li>
                            <?php
                            
                      }else*/if(in_array($user->friend, $rFriend)){
                           echo "<li class='item_list list-group-item'>"
                        . "$user->friend " ?>
                            <button disabled class='buttonUser btn btn-outline-primary' type='button'>
                                <i class="fas fa-clock"></i>
                            </button>
                            </li>
                            <?php
                      
    
                      }else{
                        echo "<li class='item_list list-group-item'>"
                        . "$user->friend " ?>
                            <button class='buttonUser btn btn-outline-danger' type='button' onclick="window.location='<?php echo site_url('VisageLivre/rmRelation/'.$user->friend);?>'">
                                <i class='fas fa-user-times'></i>
                            </button>
                            </li>
                            <?php
                        
                      }
                        }
                    }
                }
            ?>
            <?php /*
                foreach ($allUser as $user){
                    if($user->nickname!=$this->session->userdata('nickname')){
                        echo "<li class='item_list list-group-item'>"
                        . "$user->nickname " ?>
                            <button class='buttonUser btn btn-outline-danger' type='button' onclick="window.location='<?php echo site_url('VisageLivre/rmRelation/'.$user->nickname);?>'">
                                <i class='fas fa-user-times'></i>
                            </button>
                            </li>
                            <?php
                    }
                }
           */ ?>
        </ul>
        </div>
        <div class="row">
            <a class="col btn btn-primary btn-sm" id="button_l" href="<?php echo site_url('VisageLivre/home/') ?>" role="button">Utilisateurs</a>
            <a class="col btn btn-primary btn-sm" id="button_r" href="<?php echo site_url('VisageLivre/home/vue/friend'); ?>" role="button">Amis</a>
        </div>
</aside>
