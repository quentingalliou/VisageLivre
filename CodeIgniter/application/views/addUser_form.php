<div class="col-12">
    <h1>Bienvenue sur VisageLivre</h1>
</div>
<div class="col-4 offset-1 div_co" id="form_co">
    <h4>S'inscrire</h4>
    <?php echo form_open('VisageLivre/addUser'); ?>
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">Pseudo</span>
        </div>
        <input type="text" name ="nickname" class="form-control" aria-label="Pseudo" aria-describedby="basic-addon1">
        <?php echo form_error('nickname'); ?>
    </div>
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">Mot de passe</span>
        </div>
        <input type="password" name ="password" class="form-control" aria-label="Password" aria-describedby="basic-addon1">
        <?php echo form_error('password'); ?>
    </div>
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">Mot de passe</span>
        </div>
        <input type="password" name ="passconf" class="form-control" aria-label="Password" aria-describedby="basic-addon1">
        <?php echo form_error('passconf'); ?>
    </div>
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">Email</span>
        </div>
        <input type="email" name ="email" class="form-control" aria-label="Email" aria-describedby="basic-addon1">
        <?php echo form_error('email'); ?>
    </div>
        <div>
            <input class="btn btn-success" type="submit" value="S'inscrire" />
            <p>Vous avez déjà un compte ? <a href="<?php echo site_url('VisageLivre/connectUser');?>">Se connecter</a>
        </div>
    </form>
</div>
<div class="offset-1 col-4 div_co texte">
    <p>
        Vous inscrire vous permettra de pouvoir créer des billets que seul vos amis pourront lire. Et pour avoir des amis,
        il vous un compte ! Alors n'hésitez pas :) !
        <br /><br />
        <i id="grand">A tout de suite sur VisageLivre !</i>
    </p>
</div>
