<div class="col-2">
    <div>
        <h5 class="sous_titre">Mon compte</h5>
        <h6>Pseudo</h6>
        <?php echo $this->session->userdata('nickname'); ?>
    </div>
    <br />
    <div>
        <h6>Email</h6>
        <?php
        foreach ($infoUser as $info) {
            echo $info->email;
        }
        ?>
    </div>
    <br />
    <br />
    <div>
        <h6>Supprimer mon compte</h6>
        <button class='buttonUser btn btn-outline-danger' type='button' onclick="window.location = '<?php echo site_url('VisageLivre/rmUser'); ?>'">
            Suppression
        </button>
    </div>
</div>
<div class="col-8" id="mesPostes">
    <h5>Mes billets</h5>
    <div class="liste">
        <?php
            foreach($post as $p){ ?>
                <div class="form-inline input-group mb-3">
                    <div class="input-group-prepend">
                       <?php echo  "<span class='input-group-text'><b>$p->auteur<br />". substr($p->create_date, 0, -10) . "</b></span>";?>
                    </div>
                    <p type="text" class="form-control" aria-label="Mon billet" aria-describedby="basic-addon2">
                        <?php 
                        if(strlen($p->substring)==30){
                            echo $p->substring.'...' ;
                        } else{
                            echo $p->substring;
                        }?>
                    </p>  
                    <div class="input-group-append">
                        <button class="btn btn-outline-primary" onclick="window.location = '<?php echo site_url('VisageLivre/detailPost/').$p->iddoc; ?>'" type="button">Voir le billet    </button> <!-- Ajouter var-->
                        <button class="btn btn-outline-danger" onclick="window.location = '<?php echo site_url('VisageLivre/rmPost/').$p->iddoc; ?>'" type="button">Supprimer le billet</button> <!-- Ajouter var-->
                    </div>
               </div>
            <?php } ?>
    </div>
</div>
<div class="col-2">
    <h5 class="sous_titre">Mes amis</h5>
    <div class="liste">
        <ul class="list-group row">
            <?php
            foreach ($friend['allUser'] as $f) {
                if ($f->friend != $this->session->userdata('nickname')) {
                    echo "<li class='item_list list-group-item'>"
                    . "$f->friend ";
                    ?>
                    <button class='buttonUser btn btn-outline-danger' type='button' onclick="window.location = '<?php echo site_url('VisageLivre/rmRelation/' . $f->friend); ?>'">
                        <i class='fas fa-user-times'></i>
                    </button>
                    </li>
                    <?php
                }
            }
            ?>
        </ul>
    </div>
</div>

