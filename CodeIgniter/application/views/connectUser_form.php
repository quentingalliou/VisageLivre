<div class="col-12">
    <h1>Bienvenue sur VisageLivre</h1>
</div>
<div class="col-4 offset-1 div_co" id="form_co">
    <h4>Se connecter</h4>
    <?php echo form_open('VisageLivre/connectUser'); ?>
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">Pseudo</span>
        </div>
        <input type="text" name ="nickname" class="form-control" aria-label="Pseudo" aria-describedby="basic-addon1">
        <?php echo form_error('nickname'); ?>
    </div>
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">Mot de passe</span>
        </div>
        <input type="password" name ="password" class="form-control" aria-label="Password" aria-describedby="basic-addon1">
        <?php echo form_error('password'); ?>
    </div>
    <div>
        <input class="btn btn-success" type="submit" value="Se connecter" />
        <p>Vous n'avez pas de compte ? <a href="addUser">S'inscrire</a>
    </div>
</form>
</div>
<div class="offset-1 col-4 div_co texte">
    <p>
        <b>VisageLivre</b> est le nouveau réseau social du moment ! Venez discutez avec vos amis en publiants des 
        billets. N'hésitez pas à les commenter pour donner votre avis !
        <br /><br />
        <i id="grand">A tout de suite sur VisageLivre !</i>
    </p>
</div>
<div class="col-12 div_co" id="pre_footer">
    <p><i>Site web réalisé par Quentin GALLIOU et Corentin BOSQUET, étudiant en DUT Informatique à l'IUT de LANNION</i></p>
</div>
