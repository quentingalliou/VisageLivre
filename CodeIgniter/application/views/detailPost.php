<div class="col">
    <?php
    foreach ($post as $p) { ?>
        <div class='form-inline input-group mb-3 col-10 offset-1'  >
            <div class='input-group-prepend'>
                <?php echo "<span class='input-group-text'><b>" . $p->auteur . "<br />" . substr($p->create_date, 0, -10) . "</b></span>"; ?>
            </div>
                <p class='form-control'>
                    <?php echo "$p->content"; ?>
                </p>
                <?php if($p->auteur==$this->session->userdata('nickname')){ ?>
            <div class='input-group-append'>
                <button class='btn btn-outline-danger' onclick='window.location = " <?php echo site_url('VisageLivre/rmPost/') . $p->iddoc; ?>"' role='button'>Supprimer le billet</button>
            </div>
                <?php } ?>
        </div>
    <?php }
    if (!empty($comment)) {
        foreach ($comment as $c) { ?>
            <div class='form-inline input-group mb-3 col-8 offset-2'  >
                <div class='input-group-prepend'>
                    <?php echo "<span class='input-group-text'><b>" . $c->auteur . "<br />" . substr($p->create_date, 0, -10) . "</b></span>"; ?>
                </div>
                <p class='form-control'>
                    <?php echo "$c->content"; ?>
                </p>
                <div class='input-group-append'>
                    <?php if ($this->session->userdata('nickname')==$c->auteur) {?>
                        <button class='btn btn-outline-danger' onclick='window.location = " <?php echo site_url('VisageLivre/rmComment/') . $c->iddoc ?> "' role='button'>Supprimer le commentaire</button>
                    <?php
                    }
                    ?>
                </div>
            </div>
        <?php
        }
    }
?>
</div>
