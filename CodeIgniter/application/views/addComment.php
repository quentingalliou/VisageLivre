<div class="form-inline input-group mb-8" >
        <?php 
        $attribut = array('class' => 'col');
        echo form_open("VisageLivre/addComment/".$post[0]->iddoc, $attribut); ?>
            <div class="input-group mb-3">
                <textarea type="text" name="content" id="content" class="form-control" placeholder="Ecrivez quelque chose..." aria-label="Ecrivez quelque chose..." aria-describedby="basic-addon2"></textarea>
                <div class="input-group-append">
                    <input class="btn btn-outline-success btn-block" type="submit" value="Envoyer mon commentaire" />
                </div>
            </div>
        </form>
    </div>
