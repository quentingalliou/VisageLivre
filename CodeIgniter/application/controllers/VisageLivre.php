<?php
    class VisageLivre extends CI_Controller {
		
            private $tabFR='visagelivre._friendrequest';
            private $tabFO='visagelivre._friendof';
            private $tabUser='visagelivre._user';
            private $tabDoc='visagelivre._document';
            private $tabPost='visagelivre._post';
            private $tabCom='visagelivre._comment';
            
            private $anonymous=0;
        
        public function __construct (){
            parent :: __construct ();
            $this->load->model('VisageLivre_model', 'vlm');
            $this->load->library(array('form_validation', 'Layout','session'));
            $this->load->helper(array('url', 'assets', 'form','security'));
            $this->load->database();
            
            //$this->session->method();
            $tabFR='visagelivre._friendrequest';
            $tabFO='visagelivre._friendof';
            $tabUser='visagelivre._user';
         

            //$this->load->database();

        }
        
        public function index () {
            $this->display('connectUser_form');
        }
        
        public function display ($content = 'connectUser_form'){
            if(!file_exists('application/views/'.$content.'.php')) {
                show_404();
            }
            
            $data['content']=$content;
            $this->load->vars($data); // $data is 'extracted' and its compenents has a global access
            $this->layout->view($content);
           
        }
        
        // Permet de créer un utilisateur (vérification formulaire)
        public function addUser(){
			
            $this->form_validation->set_error_delimiters('<p class="form_erreur">', '</p>');
            
            $this->form_validation->set_rules('nickname', '"Nickname"','required|callback_nickname_check|max_length[30]|alpha_numeric'); // a verfiier pour le | avec un callback
            $this->form_validation->set_rules('password', '"Password"', 'trim|required');
            $this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required');//|matches[password]');//
            $this->form_validation->set_rules('email', '"Email"', 'trim|required');
            
            $data['nicknameTest']=$this->input->post('nickname');
            
            if ($this->form_validation->run()){
                $nickname = $this->security->xss_clean($this->input->post('nickname'));
                $pass = $this->security->xss_clean($this->input->post('password'));
                $password= password_hash($pass,PASSWORD_DEFAULT);
                $email = $this->security->xss_clean($this->input->post('email'));
                /*$utilisateur = array($nickname, $pass, $email);
                $this->VisageLivre_model->todo_add_user($utilisateur);*/
                $data=array();
                $data['nickname']=$nickname;
                $data['pass']=$password;
                $data['email']=$email;
                
                if ($this->vlm->create($this->tabUser,$data)){
                    $this->layout->view('addUser_success');
                    // Faire un redirect 
                    
                    
				}else{ show_404();}
                
               
            }else {
                 $this->layout->view('addUser_form', $data); //baseUrl
            }      
        }
        
        
        
        // Permet de verifier si le nickname est déjà utilisé
        public function nickname_check($str)
        {
                $nickname = $this->vlm->count($this->tabUser,array('nickname'=>$str));
                /*$data=array();
                $data['nickname']=$nickname;
                $this->layout->view('yolo',$data);*/
               if ($nickname!=0)
                {
                        $this->form_validation->set_message('nickname_check', "Le nom '$str' est déjà utilisé");
                        return FALSE;
                }
                else
                {
                        return TRUE;
                }
        }
        
        //Permet de connecter un utilisateur
        public function connectUser(){
            $this->form_validation->set_error_delimiters('<p class="form_erreur">', '</p>');
            $this->form_validation->set_rules('nickname', 'Nickname', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required|callback_password_check');
            
            if ($this->form_validation->run() === FALSE){
				/*$session_id = $this->session->userdata('session_id'); // ton 'session_id' c'est une clé mais t'as pas mis de valeur regarde ici si t'en as besoin https://www.codeigniter.com/user_guide/libraries/sessions.html?highlight=session
				$adresse_ip = $this->session->userdata('ip_address');
				$user_agent_du_navigateur = $this->session->userdata('user_agent');
				$derniere_visite = $this->session->userdata('last_activity');
                                
                */
                 
                
                $this->layout->view('connectUser_form'); //baseUrl
                //$this->listUser();
            }else {
		$nickname=$this->security->xss_clean($this->input->post('nickname'));
		$password=$this->security->xss_clean($this->input->post('password'));
			
		$this->session->set_userdata('nickname', $nickname);
                //$this->home();
                $this->layout->view('connect_success');
            }
        }
        
        
        //Permet de vérifier le password avec le hash de la bdd
        public function password_check($password){
			$nickname=$this->security->xss_clean($this->input->post('nickname')); // a vérifier je sais pas si le input peut etre pris tant que form validation pas validé
			$pass=$this->vlm->read($this->tabUser,'pass',array('nickname'=>$nickname));
                        if(!empty($pass)){
                        $hash=$pass['0']->pass;
			if (password_verify($password,$hash)){
				return TRUE;}
			else{ return FALSE;}
                }else {return FALSE;}
        }
						        
        //Permet de déconnecter un utilisateur
        public function disconnectUser(){
			 //  Détruit la session

			$this->session->sess_destroy();


			//  Redirige vers la page d'accueil

			redirect(site_url('VisageLivre/connectUser')); // A changer 
			
		}
		
		public function AddRelation($friend=''){
			$set=array();
			$set2=array();
			$set['nickname']=$this->session->userdata('nickname');
			$set['friend']=$friend;
			$set2['nickname']=$friend;
			$set2['friend']=$this->session->userdata('nickname');

			$verif=$this->vlm->create($this->tabFO,$set);
			$verif2=$this->vlm->create($this->tabFO,$set2);
			if($verif && $verif2){
			
				$whereU=array();
				$whereU['nickname']=$this->session->userdata('nickname');
				$whereU['target']=$friend;
				$this->vlm->delete($this->tabFR,$whereU);
				
				$whereF=array();
				$whereF['nickname']=$friend;
				$whereF['target']=$this->session->userdata('nickname');
                                $this->vlm->delete($this->tabFR,$whereF);
                                $this->home();

				
			}else { show_404();}	
		}
		

        //Permet de récupérer les infos de l'utilisateur
        public function infoUser(){
            $data=array();
            $where['nickname']=$this->session->userdata('nickname');
            $getInfo=$this->vlm->read($this->tabUser,'nickname,email',$where);
            $data['infoUser']=$getInfo;
            
            // add formulaire de creation compte
            
            //AJout corentin : voir mes posts pris de la fonction plus bas
            
            $data['post'] = $this->postUser();
            $data['friend']=$this->listUser('friend');
            
            
            $this->layout->view('infoUser',$data);
        }
        
        //Permet d'afficher la liste des utilisateurs
        public function listUser($user=null){
            $data=array();
            $where=array();
            //$allUser=array('1', '2', '3');
            $where['nickname']=$this->session->userdata('nickname');

            if ($user=='friend'){
                $data['allUser']=$this->vlm->read($this->tabFO,'friend',$where );//Vers model pour récupérer tous les membres utilisateurs et amis
                     // $data['allUser']=$this->vlm->read($this->tabUser, 'nickname');

            
            }else{
               $data['allUser']=$this->vlm->read($this->tabUser, 'nickname');
               $data['friend']=$this->vlm->read($this->tabFO,'friend',$where );
            }
            
            return $data;
            /*$data['postFriends']=$this->postFriends();
            
            $this->layout->views('demandeAmis', $data);
            $this->layout->views('addPost');
            $this->layout->views('listBillet');
            $this->layout->view('listUser_view',$data);*/
            
        }
        
        // Page d'accueil 
        public function home($fct=null,$opt=null){
            $data=array();
            $filtre=1;
            $friend=null;
            if($fct=='filtre'){
               $filtre=$opt;
            }else {$friend=$opt;}
            
            $data['listUser']=$this->listUser($friend);
            $data['postFriends']=$this->postFriends($filtre);
            $data['listRF']=$this->listRequestFriend();
            $data['myListRF']=$this->listMyRequestFriend();
            
            $this->layout->views('demandeAmis', $data);
            $this->layout->views('addPost');
            $this->layout->views('listBillet');
            $this->layout->view('listUser_view',$data);
        }
        
        //Permet d'affichier la liste des pseudos de mes demandes d'amis
        public function listMyRequestFriend(){
            $nickname=$this->session->userdata('nickname');
            $data=$this->vlm->read($this->tabFR,'target',array('nickname'=>$nickname));
            return $data;
        }
        
        //Permet d'afficher la liste des demandes d'amis
        public function listRequestFriend(){
            $nickname=$this->session->userdata('nickname');
            $data=array();
            $allRequestFriend=$this->vlm->read($this->tabFR,'nickname',array('target'=>$nickname));
            $data['allRF']=$allRequestFriend;
            return $data;
            //$this->layout->view('requestFriend_view',$data); // Mettre le h5 en lien qui redireige vers cette page
        }
        
        //Permet de demander un utilisateur en ami
        public function addRequest($target){
            $set['nickname']=$this->session->userdata('nickname'); //faire une vérif has_userdata
            $set['target']=$target;
            /*$set1['nickname']=$set['target'];
            $set1['target']=$set['nickname'];*/
            if(!$this->vlm->create($this->tabFR,$set) ){//|| !$this->vlm->create($this->tabFR,$set1)){
                show_404();
            }
            $this->home();
        }
         //Permet de refuser un utilisateur en ami
        public function rmRequest($target){
            $set['nickname']=$this->session->userdata('nickname'); //faire une vérif has_userdata
            $set['target']=$target;
            $set1['nickname']=$set['target'];
            $set1['target']=$set['nickname'];
            if(!$this->vlm->delete($this->tabFR,$set) || !$this->vlm->delete($this->tabFR,$set1)){
                show_404();}else{$this->home();}
        }
        
        // Permet de supprimer une relation
        public function rmRelation($friend=''){
			$set=array();
			$set2=array();
			$set['nickname']=$this->session->userdata('nickname');
			$set['friend']=$friend;
			$set2['nickname']=$friend;
			$set2['friend']=$this->session->userdata('nickname');

			$verif=$this->vlm->delete($this->tabFO,$set);
			$verif2=$this->vlm->delete($this->tabFO,$set2);
			if( $verif && $verif2){
				$this->infoUser(); // a changer + add pop "ami(e) supprimé(e)"
			}else{show_404();}	// a verifier 
		}
		
		// Permet de creer un billet
		public function addPost(){
			$this->form_validation->set_error_delimiters('<p class="form_erreur">', '</p>');
			$this->form_validation->set_rules('content', '"Content"', 'required|max_length[128]'); // mettre sécu XSS
			
			if($this->form_validation->run()){
				$set=array();
				if ($this->session->has_userdata('nickname')){
					$set['auteur']=$this->session->userdata('nickname');
				/*}else { // mettre un else if avec recherche d'un nickname dans cookie 
					$this->anonymous +=1;
					$set['auteur']=$this->anonymous;
					//add cookie*/
				}
				$set['content']=$this->security->xss_clean($this->input->post('content'));
				$test=$this->vlm->create($this->tabDoc,$set);
				if($test){
					$iddoc=$this->vlm->readMax($this->tabDoc,'iddoc');
                                        $iddoc=$iddoc['0']->iddoc;
					$test2=$this->vlm->create($this->tabPost,array('iddoc'=>$iddoc));
                                        //listUser();
                                        if(!$test2){show_error('insert impossible',501);}else{ $this->home();}
				}else{ show_error('probleme pour ajouter le id dans la table post',404);}
			}else{
				return listUser();
                                //$this->layout->view('listUser_view');
			}
		}
		
		//Permet de voir les billets du user
		public function postUser(){ //pagination
			$where=array();
			if($this->session->has_userdata('nickname')){
				$where['auteur']=$this->session->userdata('nickname');
			}
                        
			return $this->vlm->readJoinOrderBy($this->tabDoc,"substring(content,1,30),auteur,create_date,$this->tabPost.iddoc",$where,$this->tabPost,"$this->tabDoc.iddoc = $this->tabPost.iddoc",'create_date','DESC'); // add "..." dans la vue
		}
		
		public function postFriends($mode=1){ // si erreur mettre une valeur par default et update le switch
			$data=array();
                if($this->session->has_userdata('nickname')) {
                    if($this->vlm->countAll($this->tabFO)>0){
				$nickname=$this->session->userdata('nickname');
				$rqFriend=$this->vlm->read($this->tabFO,'friend',array('nickname'=>$nickname)); // voir si on ajoute l'utlisateur dans les news
                                if(empty($rqFriend)){return 0;}
				$friends=array();
                                foreach($rqFriend as $f){
                                    array_push($friends,$f->friend);
                                }
                                
				switch ($mode){ // look for fetch data without refreshing page 
					/*case 0:
                                                //$data['postFriends']=
                                                return $this->vlm->readCol($this->tabDoc,'substring(content,1,30),auteur,create_date','auteur',$friends,'auteur DESC'); // si ça marche pas voir ce qu'il y a dans $friends
						break;*/
					case 0:
						
                                                return $this->vlm->readColJoin($this->tabDoc,"substring(content,1,30),auteur,create_date,$this->tabPost.iddoc",'auteur',$friends,$this->tabPost,"$this->tabDoc.iddoc = $this->tabPost.iddoc",'auteur ASC, create_date DESC'); // si ça marche pas voir ce qu'il y a dans $friends
						break;
                                        case 1:
						return $this->vlm->readColJoin($this->tabDoc,"substring(content,1,30),auteur,create_date,$this->tabPost.iddoc",'auteur',$friends,$this->tabPost,"$this->tabDoc.iddoc = $this->tabPost.iddoc",'create_date', 'DESC'); // si ça marche pas voir ce qu'il y a dans $friends
						break;
					}
						
				// put content dans area en disable et date juste au dessus de l'area et nom en face
				
				///$this->layout->view('news',$data);
			//}else if nickname in cookie{}
                    }else{ return 0;}
			}else{
				show_404();
			}
		}
		
		//Permet de voir le détail d'un billet
		public function detailPost($iddoc){
			if($this->session->has_userdata('nickname')){
                            $nickname=$this->session->userdata('nickname');
                        }//else cookie
				
                        $auteurPost=$this->vlm->read($this->tabDoc,'auteur',array('iddoc'=>$iddoc));
                        $auteurPost=$auteurPost[0]->auteur;
			$comment=$this->vlm->read($this->tabCom,'iddoc',array('ref'=>$iddoc));
                        $data['com']=$comment;
                     	//$this->layout->view('detailPost',$data);
                        if(!empty($comment)){
                            $idcom=array();
                            foreach($comment as $c){
                                array_push($idcom,$c->iddoc);
                                
                            }
                            $auteurCom=$this->vlm->readCol($this->tabDoc,'auteur','iddoc',$idcom);
                            $idAuteurCom=array();
                            foreach($auteurCom as $c){
                                array_push($idAuteurCom,$c->auteur);
                            }
                            if (in_array($nickname,$idAuteurCom)){
				$rmBoutonCom=$this->vlm->readJoin($this->tabDoc,"$this->tabCom.iddoc",array(),"$this->tabCom","$this->tabCom.ref = $this->tabDoc.iddoc"); // pas sur du tout je suis pressé 
                                $auteurC=array();
                                foreach($rmBoutonCom as $c){
                                    array_push($auteurC,$c->iddoc);
                                
                                }
                                $rmBtnC=$this->vlm->readCol($this->tabDoc,'auteur','iddoc',$auteurC);
                                $rmC=array();
                                foreach($rmBtnC as $c){
                                    array_push($rmC,$c->auteur);
                                
                                }
                                $data['rmBtnC']=$rmC;
                            }
                            $data['comment']=$this->vlm->readCol($this->tabDoc,'*','iddoc',$idcom); // lire le .9 par rapport à l'affichage des commentaires

                        }
			if( $nickname==$auteurPost){
                            $data['rmBoutonPost']=1;
                        }
			
			$data['post']=$this->vlm->read($this->tabDoc,'*',array('iddoc'=>$iddoc));
			$this->layout->views('detailPost',$data);
                        $this->layout->view('addComment',$data);
                                   
                
                                   
		}
		
		// Permet de creer un commentaire pour un billet
		public function addComment($iddoc){
			
			if( $this->session->has_userdata('nickname')){
				$nickname=$this->session->userdata('nickname');
			}//else if cookie
			$auteur=$this->vlm->read($this->tabDoc,'auteur',array('iddoc'=>$iddoc));
			$auteur=$auteur[0]->auteur;
                        $friends=$this->vlm->read($this->tabFO,'friend',array('nickname'=>$auteur)); // verifier le format de $auteur
			$listF=array();
                            foreach($friends as $f){
                                array_push($listF,$f->friend);
                            }
                        if ( ($auteur==$nickname) || in_array($nickname,$listF)){//nickname == auteur ou friend de auteur) in_array
				$this->form_validation->set_error_delimiters('<p class="form_erreur">', '</p>');
				$this->form_validation->set_rules('content', '"Content"', 'required|max_length[128]'); // mettre sécu XSS
				if($this->form_validation->run()){
					$set=array();
					$set['auteur']=$nickname;
					$set['content']=$this->security->xss_clean($this->input->post('content'));
					$this->vlm->create($this->tabDoc,$set);
					$idcom=$this->vlm->readMax($this->tabDoc,'iddoc');
					$set2=array();
					$set2['iddoc']=$idcom[0]->iddoc;
					$set2['ref']=$iddoc;
                                        if($this->vlm->create($this->tabCom,$set2)){$this->detailPost($iddoc);}
                                        else {show_404();}//@FIXME pas d'instruction ?
				}else {  show_404();}		
			}else { $this->layout->view('detailPost'); }// add info "vous n'êtes ni l'auteur ni l'un de ses amis
		}
		
		public function rmPost($iddoc,$rmuser=''){
			 $iddocC=array();
		if($rmuser!='rmuser'){	
                    $comment=$this->vlm->read($this->tabCom,'iddoc',array('ref'=>$iddoc));
                       
                                foreach($comment as $c){
                                    array_push($iddocC,$c->iddoc);
                                }
                

			$this->vlm->delete($this->tabCom,array('ref'=>$iddoc));
                        if(!empty($iddocC)){$this->vlm->deleteCol($this->tabDoc,'iddoc',$iddocC);} // verifier format de $comment
                }
                    	$this->vlm->delete($this->tabPost,array('iddoc'=>$iddoc));
			$this->vlm->delete($this->tabDoc,array('iddoc'=>$iddoc));
                        if($rmuser!='rmuser'){
                            $this->infoUser();
                        }
			
		}
		
		

		public function rmComment($iddoc,$rmUser=''){
			$nickname=$this->session->userdata('nickname');
                        $where=array();
                        $where['iddoc']=$iddoc;
                       /*$data=array();
                       $data['auteurdoc']=$auteurdoc;
                       $this->layout->view('yolo',$data);*/
			
                                 $post=$this->vlm->read($this->tabCom,'ref',array('iddoc'=>$iddoc));

				$subComment= $this->vlm->read($this->tabCom,'iddoc',array('ref'=>$iddoc));
                                $auteursubdoc=array();
                                foreach($subComment as $f){
                                    array_push($auteursubdoc,$f->iddoc);
                                }
				if (!empty($subComment)){
					$this->vlm->deleteCol($this->tabDoc,'iddoc',$auteursubdoc);
				}

                                if(!$this->vlm->delete($this->tabDoc,array('iddoc'=>$iddoc))){show_404();}
			if($rmUser!='rmuser'){
                            
                            $this->detailPost($post[0]->ref);
                        }
			
		}

		public function rmUser(){
			$nickname=$this->session->userdata('nickname');
                        $where=array();
                        $where['auteur']=$nickname;
			$docUser=$this->vlm->read($this->tabDoc,'iddoc',$where);
                        if(!empty($docUser)){
                        $iddoc=array();
                                foreach($docUser as $f){
                                    array_push($iddoc,$f->iddoc);
                                }
			$comUser=$this->vlm->readColJoin($this->tabDoc,"$this->tabCom.iddoc","$this->tabCom.ref",$iddoc,$this->tabCom,"$this->tabDoc.iddoc = $this->tabCom.iddoc");
                       /* $where['iddoc']=$iddoc;
                        $where['comUser']=$comUser;
                        $this->layout->view('yolo',$where);}*/
                        if(!empty($comUser)){
			foreach ($comUser as $com){
				$this->rmComment($com->iddoc,'rmuser');
			}
			$postUser=$this->vlm->readColJoin($this->tabDoc,"$this->tabPost.iddoc","$this->tabDoc.iddoc",$iddoc,$this->tabPost,"$this->tabDoc.iddoc = $this->tabPost.iddoc");
                        if(!empty($postUser)){
			foreach ($postUser as $post){
				$this->rmPost($post->iddoc,'rmuser');
                        }
			}
                        }
                        }
                        if(!empty($this->vlm->read($this->tabFR,'*',array('nickname'=>$nickname)))){
                            $this->vlm->delete($this->tabFR,array('nickname'=>$nickname));}
                        if(!empty($this->vlm->read($this->tabFR,'*',array('target'=>$nickname)))){
                            $this->vlm->delete($this->tabFR,array('target'=>$nickname));}
                        if(!empty($this->vlm->read($this->tabFO,'*',array('nickname'=>$nickname)))){
                            $this->vlm->delete($this->tabFO,array('nickname'=>$nickname));}
                        if(!empty($this->vlm->read($this->tabFO,'*',array('friend'=>$nickname)))){
                            $this->vlm->delete($this->tabFO,array('friend'=>$nickname));}
                        
                        $whereU['nickname']=$nickname;
			$this->vlm->delete($this->tabUser,$whereU);
                        $this->disconnectUser();
			
		}
                
                function demerde(){
                    $data['test']=$this->vlm->read($this->tabUser);
                    $this->layout->view('infoUser', $data);
                }
	}
	
	
	/*verifier toutes les set_rules par rapport au contrainte de la BDD exemple username max_length[30]
	 * Faire de la sécu : https://www.codeigniter.com/user_guide/general/security.html 
	 * afficher un message dès qu'une action est faite exemple "demande envoyée" idée : popup ou css add text(hidden) ou add text on the top during 3secand disappear  */
?>

