# OpenFoodFacts

## Tutorial Git:

* Clone

```
git clone git@gitlab.com:kentiindu29/VisageLivre.git
```

* Pull

```
git pull
```

* Push

```
git add --all
git commit -am "Commit message"
git push origin master
```

## Authors:
* **Corentin BOSQUET** - [CorentinB](https://gitlab.com/CorentinB)
* **Quentin GALLIOU** - [kentiindu29](https://gitlab.com/kentiindu29)

## License
Copyright © 2017, [Corentin Bosquet](https://gitlab.com/CorentinB) & [Quentin Galliou](https://gitlab.com/kentiindu29)
Contact us if you want to use this project and we will authorize you.
